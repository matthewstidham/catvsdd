#!/bin/bash

SIZE="$1"
if test -f "output.txt"; then
  rm output.txt
fi
touch output.txt
myfunction () {
  echo "$1"
  echo "$1" >> output.txt
  { time dd if="$1" of=10.file bs=1M count="$SIZE"; } 2>&1 | cat >> output.txt

  echo "================" >> output.txt
  echo "$1 cat" >> output.txt
  { time cat 10.file > u30.file; } 2>&1 | cat >> output.txt
  echo "================" >> output.txt
  echo "$1 cp" >> output.txt
  { time cp 10.file u20.file; } 2>&1 | cat >> output.txt
  echo "================" >> output.txt
  echo "$1 dd" >> output.txt
  { time dd if=10.file of=u40.file; } 2>&1 | cat >> output.txt
}

myfunction /dev/urandom
echo "" >> output.txt
echo "=/=/=/=/=/=/=/=/=/=/=/=/=/=/" >> output.txt
echo "" >> output.txt
echo "=/=/=/=/=/=/=/=/=/=/=/=/=/=/" >> output.txt
myfunction /dev/random
echo "" >> output.txt
echo "=/=/=/=/=/=/=/=/=/=/=/=/=/=/" >> output.txt
myfunction /dev/null

rm ./*.file

echo "Done generating data."

python generate_csv.py

echo "Done"
