#!/usr/bin/env python3

import pandas as pd


def main():
    with open('output.txt') as file:
        file = file.read()
    results = file.split('================')
    final = {}
    for result in results:
        result = result.strip('\n').split('\n')
        function = {'name': result[0]}
        for x in result:
            for stat in ['real', 'user', 'sys']:
                if stat in x:
                    function[stat] = x.split('\t')[-1]
        final[result[0]] = function
    pd.DataFrame(final).to_csv('result.csv')


if __name__ == "__main__":
    main()
